# Misc. Administrative Stuff

A place for the various tools, scripts, and configs used to maintain and release kual-system.

Such things as configuration files:

* Busybox .config => as busybox<version\>-config
* Kernel .config(s) => as:
* 2.6.22.19-lab-126/.config => 2.6.22-config
* 2.6.26-rt-lab126/.config => 2.6.26-config
* 2.6.31-rt11-lab126/.config => 2.6.31-config

Scripting use to create compressed file system trees for releases

Scripting to maintain kernel modules in triplicate sets.

Whatever - things we need for administrative tasks in the repo.
