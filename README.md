# OVERVIEW README

This file system structure supports the Kindle Unified Application Launcher (KUAL, pronounced: cool).

KUAL works on all e-ink Kindle models except for the original model (K1) and some very early firmware Kindle 2's (The latter can be easily updated)

The root of this structure (extensions/system) is on that part of the Kindle's file system that is exported as the USB storage area to the user.

This file system is also accessible internally as part of the Linux file system tree as: /mnt/us/extensions/system

The notation used here of: **'\*/'** indicates either the internal path or the external USB storage path.

This file system structure is a simplification of the Linux FHS.

Overview at: http://www.thegeekstuff.com/2010/09/linux-file-system-structure/

Reference at: http://www.pathname.com/fhs/

The simplifications here are:

/bin, /sbin, /usr/bin, /usr/sbin, /usr/local/bin, /usr/local/sbin are all consolidated as: \*/bin

/lib, /usr/lib, /usr/local/lib are all consolidated as: \*/lib

Over time, the e-ink Kindles have used different Freescale SoC devices.

Our \*/bin and \*/lib reflect these differences.

The \*/bin and \*/lib are the generic directory levels.

The \*/bin/\`uname -m\` and \*/lib/\`uname -m\` are files specific to either the armv6l or armv7l core used by the SoC.

Over time, the e-ink Kindles have used different Linux kernel versions.

This is reflected in the path to the loadable kernel modules as: \*/lib/modules/\`uname -r\`

This file structure plan presumes a "read-only" \*/etc directory used as a system wide, resource directory. It has sub-directories specific to the system resources provided.

For run-time generated files, this file system structure supports three distinctions of \*/var

\*/var/tmp - implemented in tmpfs - does not survive re-boots.

\*/var/run - implemented in tmpfs - does not survive re-boots.

\*/var/log (and other sub-directories of \*/var) are persistent.

Copyrights: Copyright \(c\) is held by the original author unless otherwise stated in the author's contribution.

License: All material here is licensed under the MIT License unless otherwise stated in the author's contribution. See: http://opensource.org/licenses/MIT

Contact the team if you have any ideas for the project
